package org.example;

import okhttp3.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) throws IOException {

        OkHttpClient client = new OkHttpClient().newBuilder()
                .callTimeout(10000, TimeUnit.SECONDS)
                .connectTimeout(10000, TimeUnit.SECONDS)
                .readTimeout(10000, TimeUnit.SECONDS)
                .writeTimeout(10000, TimeUnit.SECONDS)
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "" +
                "{\n" +
                "  \"messages\": [\n" +
                "        {\n" +
                "                \"role\": \"system\",\n" +
                "                \"content\": \"#### 定位\\n- 智能助手名称 ：文本指标拆分专家\\n- 主要任务 ：对输入的文本进行指标名称提取,指标内容提取,指标值提取,指标考核单位提取.如果语义中存在多个指标拆分为多个指标\\n\\n#### 能力\\n- 文本分析 ：能够准确分析指标,提取10文本以内的指标名称,指标考核内容,如果是量化指标提取指标值,考核部室。如果存在多个指标的语义,拆分出多个指标 \\n#### 使用说明\\n- 输入 ：一段指标考核内容。\\n- 输出 ：指标名称,指标内容,指标值,考核部室，不需要额外解释。\"\n" +
                "        },\n" +
                "        {\n" +
                "                \"role\": \"user\",\n" +
                "                \"content\": \"扎实开展本单位群众性质量活动和“质量月活动”，争创市级以上优秀质量管理（QC）小组不少于3个、质量信得过班组不少于1个。\"\n" +
                "        }\n" +
                "    ],\n" +
                "  \"model\": \"deepseek-reasoner\",\n" +
                "  \"frequency_penalty\": 0,\n" +
                "  \"max_tokens\": 2048,\n" +
                "  \"presence_penalty\": 0,\n" +
                "  \"response_format\": {\n" +
                "    \"type\": \"text\"\n" +
                "  },\n" +
                "  \"stop\": null,\n" +
                "  \"stream\": false,\n" +
                "  \"stream_options\": null,\n" +
                "  \"temperature\": 1,\n" +
                "  \"top_p\": 1,\n" +
                "  \"tools\": null,\n" +
                "  \"tool_choice\": \"none\",\n" +
                "  \"logprobs\": false,\n" +
                "  \"top_logprobs\": null\n" +
                "}");
        Request request = new Request.Builder()
                .url("https://api.deepseek.com/chat/completions")
                .method("POST", body)
                .addHeader("Content-Type", "application/json")
                .addHeader("Accept", "application/json")
                .addHeader("Authorization", "Bearer sk-c0f4c9057cf94e00bcd9e40e8ce74054")
                .build();
        Response response = client.newCall(request).execute();
        System.out.println(response.body().string());
    }
}
