package com.liuzhiqiang.shiro.jwt.controller;

import com.liuzhiqiang.shiro.jwt.config.JWTToken;
import com.liuzhiqiang.shiro.jwt.config.JwtTokenUtil;
import com.liuzhiqiang.shiro.jwt.config.UserExt;
import com.liuzhiqiang.shiro.jwt.vo.AjaxResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class LoginController {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @PostMapping("/login")
    public AjaxResult login(String username, String password) {
        UserExt userExt = new UserExt();
        userExt.setUsername(username);
        userExt.setPassword("123456");
        userExt.setRoleId("1");
        if (!StringUtils.equals(password, userExt.getPassword())) {
            throw new RuntimeException("密码不正确");
        }

        String token = jwtTokenUtil.generateToken(userExt, 600000l);

        Subject subject = SecurityUtils.getSubject();
        subject.login(new JWTToken(token));


        return AjaxResult.successData(token);
    }

    @PostMapping("/logout")
    public AjaxResult logout() {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return AjaxResult.success();
    }

}
