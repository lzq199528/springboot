package com.liuzhiqiang.shiro.jwt.config;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JWTTokenFilter extends BasicHttpAuthenticationFilter {

    /**
     * 登录标识
     */
    private static final String LOGIN_SIGN = "Authorization";

    /**
     * 检测用户是否登录 检测header里面是否包含登录标识（LOGIN_SIGN）
     *
     * @param request  request
     * @param response response
     * @return boolean
     * @author zhb
     * @date 2020/09/23 19:49
     */
    @Override
    protected boolean isLoginAttempt(ServletRequest request, ServletResponse response) {
        HttpServletRequest req = (HttpServletRequest) request;
        String authorization = req.getHeader(LOGIN_SIGN);
        return StringUtils.isNotBlank(authorization);
    }

    @Override
    protected boolean executeLogin(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest req = (HttpServletRequest) request;
        String header = req.getHeader(LOGIN_SIGN);
        JWTToken JWTToken = new JWTToken(header);
        super.getSubject(request, response).login(JWTToken);

        return true;
    }

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        if (isLoginAttempt(request, response)) {
            try {
                executeLogin(request, response);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("登录权限不足");
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * 对跨域提供支持
     *
     * @param request
     * @param response
     * @return boolean
     * @author zhb
     * @date 2020/09/23 19:58
     */
    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        httpServletResponse.setHeader("Access-control-Allow-Origin", httpServletRequest.getHeader("Origin"));
        httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PUT,DELETE");
        httpServletResponse.setHeader("Access-Control-Allow-Headers", httpServletRequest.getHeader("Access-Control-Request-Headers"));
        // 跨域时会首先发送一个option请求，这里我们给option请求直接返回正常状态
        if (httpServletRequest.getMethod().equals(RequestMethod.OPTIONS.name())) {
            httpServletResponse.setStatus(HttpStatus.OK.value());
            return false;
        }
        Boolean on = super.preHandle(request, response);
        if (!on) {
            throw new RuntimeException("认证异常");
        }
        return true;
    }
}
