package com.example;

import lombok.extern.slf4j.Slf4j;

/**
 * @author liuzhiqiang
 */
@Slf4j
public class ProgramProcessorImpl implements ProgramProcessor {

    @Override
    public String getBeanName() {
        return "test001";
    }

    @Override
    public Object programFunction(Object parameter) {
        log.info("程序开始处理");
        for (String beanDefinitionName : ApplicationContextProvider.getApplicationContext().getBeanDefinitionNames()) {
            log.info(beanDefinitionName);
        }
        log.info("程序结束处理");
        return "xxxxxxx";
    }

}
