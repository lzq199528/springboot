package com.example;

/**
 * @author liuzhiqiang
 */
public interface ProgramProcessor {

    /**
     * getBeanName
     *
     * @return
     */
    String getBeanName();

    /**
     * 处理程序
     *
     * @param parameter
     * @return
     */
    Object programFunction(Object parameter);
}
