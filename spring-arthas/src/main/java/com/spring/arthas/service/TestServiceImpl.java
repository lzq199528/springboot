package com.spring.arthas.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author liuzhiqiang
 */
@Service
@Slf4j
public class TestServiceImpl implements TestService {
    @Override
    public void test() {
        log.info("插入代码块占位");
        log.info("测试");
    }
}
