package com.spring.arthas.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @author liuzhiqiang
 */
@Controller
@Slf4j
public class StatController {

    @RequestMapping(value = "/api/stat")
    @ResponseBody
    public Map<String, Object> execute(@RequestParam(value = "ip", required = true) String ip,
                                       @RequestParam(value = "version", required = true) String version,
                                       @RequestParam(value = "command", required = true) String command,
                                       @RequestParam(value = "arguments", required = false, defaultValue = "") String arguments) {

        log.info("arthas stat, ip: {}, version: {}, command: {}, arguments: {}", ip, version, command, arguments);
        Map<String, Object> result = new HashMap<>(1);
        result.put("success", true);
        return result;
    }
}
