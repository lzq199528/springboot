package com.spring.arthas;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author liuzhiqiang
 */
@SpringBootApplication
@Slf4j
public class ArthasApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ArthasApplication.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("ArthasApplication服务器已启动完毕");
    }
}
