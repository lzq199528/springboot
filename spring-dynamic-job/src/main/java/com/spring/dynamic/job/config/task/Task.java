package com.spring.dynamic.job.config.task;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

/**
 * @author liuzhiqiang
 */
@Data
@Slf4j
public class Task implements Runnable{

    /**
     * 任务名称
     */
    private String taskName;


    /**
     * 任务描述
     */
    private String taskDescribe;


    /**
     * 下次执行时间
     */
    private Date nextExecutionTime;

    /**
     * 参数
     */
    private Map<String, Object> parameter;

    /**
     * 表达式
     */
    private String corn;

    /**
     * 任务运行状态 true 运行中 false 非运行
     */
    private Boolean taskRunState = false;


    /**
     * 任务类型
     * 1 本地任务
     * 2 远程任务
     */
    private Integer taskType;

    /**
     * 本地任务状态
     * 1 正常
     * 2 不存在
     */
    private Integer nativeTask;

    /**
     * 远程任务类型
     * 1 http
     * 2 webService
     */
    private Integer remoteTaskType;

    /**
     * http远程任务连接
     */
    private String httpUrl;

    /**
     * scheduledFuture
     */
    private ScheduledFuture scheduledFuture;

    /**
     * 运行程序
     */
    private TaskRunService taskRunService;

    @Override
    public void run() {
        log.info("任务:{}", this);
    }
}
