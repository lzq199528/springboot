package com.spring.dynamic.job.config.task;

/**
 * @author liuzhiqiang
 */
@FunctionalInterface
public interface TaskRunService {

    /**
     * 运行
     * @param task
     */
    void run(Task task);

}
