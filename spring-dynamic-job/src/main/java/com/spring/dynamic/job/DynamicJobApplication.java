package com.spring.dynamic.job;


import com.spring.dynamic.job.config.ScheduleTaskConfigurer;
import com.spring.dynamic.job.config.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.TriggerContext;

import java.util.Date;

/**
 * @author liuzhiqiang
 */
@SpringBootApplication
@Slf4j
public class DynamicJobApplication implements CommandLineRunner {

    @Autowired
    private TaskService taskService;

    public static void main(String[] args) {
        SpringApplication.run(DynamicJobApplication.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("DynamicJobApplication服务器已启动完毕");
    }
}
