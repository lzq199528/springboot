package com.spring.dynamic.job.config.task;

import com.spring.dynamic.job.config.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author liuzhiqiang
 */
@Slf4j
@Component
public class TaskOne implements TaskRunService{

    @Override
    public void run(Task task) {
        task.setTaskRunState(true);
        log.info("开始任务:{}",task.getTaskName());
        log.info("运行任务:{}", task);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("结束任务:{}",task.getTaskName());
        task.setTaskRunState(false);
    }

    @PostConstruct
    public void init() {
        TaskService.runServiceMap.put("test",this);
    }
}
