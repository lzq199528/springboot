package com.spring.batch.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

/**
 * @author liuzhiqiang
 */
@Slf4j
@Component
public class JobListener implements JobExecutionListener {

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Override
    public void beforeJob(JobExecution jobExecution) {
        log.info("任务执行前");
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        log.info("任务执行后");
    }
}
