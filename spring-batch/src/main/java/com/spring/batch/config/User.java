package com.spring.batch.config;

import lombok.Data;

import java.util.Random;

/**
 * @author liuzhiqiang
 */
@Data
public class User {

    private String username;

    private String password;

    private Integer age;

    public User() {
        this.username = String.valueOf(Math.random());
        this.password = String.valueOf(Math.random());
        this.age = new Random().nextInt(100);
    }
}
