package com.spring.kafka.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author liuzhiqiang
 */
@Data
public class User implements Serializable {

    private String name;

    private Integer age;

    private Integer birthday;
}
