package com.spring.kafka;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

/**
 * 开发公司：联信
 * 版权：联信
 * <p>
 * Annotation
 *
 * @author 刘志强
 * @created Create Time: 2021/5/25
 */
@SpringBootApplication
@Slf4j
@Component
public class KafkaApplication implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(KafkaApplication.class, args);
    }


    @Override
    public void run(String... args) {
        log.info("服务已经起动");
    }


}


