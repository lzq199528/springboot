package com.spring.aop.config.aop;

import lombok.NonNull;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.aop.support.ComposablePointcut;
import org.springframework.aop.support.annotation.AnnotationMatchingPointcut;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;

/**
 * @author liuzhiqiang
 */
public class CustomPointcutAdvisor  extends AbstractPointcutAdvisor implements
        BeanFactoryAware {

    private Advice advice;

    private Pointcut pointcut;

    public CustomPointcutAdvisor(@NonNull CustomMethodInterceptor customMethodInterceptor) {
        this.advice = customMethodInterceptor;
        this.pointcut = buildPointcut();
    }

    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    @Override
    public Advice getAdvice() {
        return this.advice;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        if (this.advice instanceof BeanFactoryAware) {
            ((BeanFactoryAware) this.advice).setBeanFactory(beanFactory);
        }
    }

    private Pointcut buildPointcut() {
//        String traceExecution = "@annotation(com.spring.aop.config.aop.CustomAnnotation)";
//        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
//        pointcut.setExpression(traceExecution);
//        return pointcut;
        Pointcut cpc = new AnnotationMatchingPointcut(CustomAnnotation.class, true);
        Pointcut mpc = AnnotationMatchingPointcut.forMethodAnnotation(CustomAnnotation.class);
        return new ComposablePointcut(cpc).union(mpc);
    }
}
