package com.spring.aop.config.aop;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author liuzhiqiang
 */
@Configuration
public class InterceptorConfig {
    @Bean
    public CustomPointcutAdvisor customPointcutAdvisor() {
        CustomMethodInterceptor interceptor = new CustomMethodInterceptor();
        CustomPointcutAdvisor advisor = new CustomPointcutAdvisor(interceptor);
        advisor.setOrder(1);
        return advisor;
    }
}
