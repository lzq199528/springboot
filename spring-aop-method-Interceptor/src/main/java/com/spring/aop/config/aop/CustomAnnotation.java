package com.spring.aop.config.aop;

import java.lang.annotation.*;

/**
 * @author liuzhiqiang
 */
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CustomAnnotation {

    String value() default "";
}
