package com.spring.aop.service;

import org.springframework.stereotype.Service;

/**
 * @author liuzhiqiang
 */
@Service
public class TestServiceImpl implements TestService{
    @Override
    public String getName() {
        throw new RuntimeException("xxx");
    }
}
