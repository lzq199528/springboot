package com.spring.aop.service;

import com.spring.aop.config.aop.CustomAnnotation;

/**
 * @author liuzhiqiang
 */
public interface TestService {

    /**
     * getName
     * @return
     */

    @CustomAnnotation("xxx")
    public String getName();
}
