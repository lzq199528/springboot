package com.spring.http.basic.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuzhiqiang
 */
@RestController
@RequestMapping("/realm1")
@Slf4j
public class HttpBasicController {

    @GetMapping("/test")
    public String test() {
        return "xxxxxx";
    }
}
