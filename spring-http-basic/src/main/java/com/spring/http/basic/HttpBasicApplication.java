package com.spring.http.basic;


import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * @author liuzhiqiang
 */
@SpringBootApplication
@ServletComponentScan
@Slf4j
public class HttpBasicApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(HttpBasicApplication.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("HttpBasicApplication服务器已启动完毕");
    }
}
