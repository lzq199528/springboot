package com.liuzhiqiang.security.confing;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * @author 刘志强
 * @title: UserDetailsServiceImpl
 * @projectName housekeeper-data-center-back
 * @description: TODO
 * @date 2021/7/2822:26
 */
@Service("userDetailsService")
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * @param userName
     * @return
     */
    @Override
    public User loadUserByUsername(String userName) {
        log.info("进入loadUserByUsername");
        User user = new User();
        user.setUsername(userName);
        if (StringUtils.equals(userName, "admin")) {
            throw new UsernameNotFoundException("未找到用户");
        }
        user.setPassword(passwordEncoder.encode("123456"));
        List<UserPermission> list = new ArrayList<>();
        list.add(new UserPermission("admin"));
        user.setList(list);
        log.info("出loadUserByUsername");
        return user;
    }
}
