package com.liuzhiqiang.security.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@Slf4j
public class TestController {

    @GetMapping("/test")
    @CrossOrigin
    public String test() {
        log.info("当前登录用户：{}", SecurityContextHolder.getContext().getAuthentication());
        return "测试 需要认证访问的接口";
    }


    @GetMapping("/test1")
    @PreAuthorize("hasAnyAuthority('admin')")
    @CrossOrigin
    public String test1() {
        log.info("当前登录用户：{}", SecurityContextHolder.getContext().getAuthentication());
        return "测试 已授权访问的接口";
    }

    @GetMapping("/test2")
    @PreAuthorize("hasAnyAuthority('user')")
    @CrossOrigin
    public String test2() {
        log.info("当前登录用户：{}", SecurityContextHolder.getContext().getAuthentication());
        return "测试 未授权访问的接口";
    }

    @GetMapping("/test3")
    @CrossOrigin
    public String test3() {
        log.info("当前登录用户：{}", SecurityContextHolder.getContext().getAuthentication());
        return "测试 可匿名访问的接口";
    }

    @GetMapping("/loginPage")
    public ModelAndView loginPage() {
        return new ModelAndView("/login");
    }

    @GetMapping("/index")
    public ModelAndView index() {
        return new ModelAndView("/index");
    }

    @GetMapping("/test5")
    public String test5() {
        log.info("当前登录用户：{}", SecurityContextHolder.getContext().getAuthentication());
        return "测试 scopes USER";
    }
}
