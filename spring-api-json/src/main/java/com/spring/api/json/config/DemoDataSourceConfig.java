package com.spring.api.json.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * 数据源配置，对应 application.yml 的数据库连接池 datasource 配置。
 * 也可以直接 new 再 set 属性，具体见 Druid 的 DbTestCase
 * https://github.com/alibaba/druid/blob/master/src/test/java/com/alibaba/druid/DbTestCase.java
 *
 * @author Lemon
 */
@Configuration
public class DemoDataSourceConfig {

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.druid")
    public DruidDataSource druidDataSource() {
        return new DruidDataSource();
    }

}
