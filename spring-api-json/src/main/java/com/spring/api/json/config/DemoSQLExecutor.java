package com.spring.api.json.config;

import apijson.framework.APIJSONSQLExecutor;
import apijson.orm.SQLConfig;
import com.spring.api.json.ApiJoinApplication;

import javax.sql.DataSource;
import java.sql.Connection;


/**
 * SQL 执行器，支持连接池及多数据源
 * 具体见 https://github.com/Tencent/APIJSON/issues/151
 *
 * @author Lemon
 */
public class DemoSQLExecutor extends APIJSONSQLExecutor {
    public static final String TAG = "DemoSQLExecutor";

    // 适配连接池，如果这里能拿到连接池的有效 Connection，则 SQLConfig 不需要配置 dbVersion, dbUri, dbAccount, dbPassword
    @Override
    public Connection getConnection(SQLConfig config) throws Exception {
        //		Log.d(TAG, "getConnection  config.getDatasource() = " + config.getDatasource());

        String key = config.getDatasource() + "-" + config.getDatabase();
        Connection c = connectionMap.get(key);
        if (c == null || c.isClosed()) {
            DataSource ds = ApiJoinApplication.getApplicationContext().getBean(DataSource.class);
            // 另一种方式是 DemoDataSourceConfig 初始化获取到 Datasource 后给静态变量 DATA_SOURCE 赋值： ds = DemoDataSourceConfig.DATA_SOURCE.getConnection();
            connectionMap.put(key, ds == null ? null : ds.getConnection());
        }

        // 必须最后执行 super 方法，因为里面还有事务相关处理。
        // 如果这里是 return c，则会导致 增删改 多个对象时只有第一个会 commit，即只有第一个对象成功插入数据库表
        return super.getConnection(config);
    }

}
