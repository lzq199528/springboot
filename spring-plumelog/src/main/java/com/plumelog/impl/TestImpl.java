package com.plumelog.impl;

import com.plumelog.trace.annotation.Trace;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author liuzhiqiang
 */
@Component
@Slf4j
public class TestImpl {

    @Trace
    public void logPrint() {
        log.info("你好");
        log.info("你好2");
        // throw new RuntimeException("我异常了");
    }

    @Async
    public void asyncTest() {
        log.info("asyncTest");
        throw new RuntimeException("asyncTest异常");
    }
}
