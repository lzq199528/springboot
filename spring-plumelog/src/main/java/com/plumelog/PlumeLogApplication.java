package com.plumelog;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author liuzhiqiang
 */
@SpringBootApplication
@EnableAsync
@Slf4j
public class PlumeLogApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(PlumeLogApplication.class, args);
    }

    public static AtomicLong atomicLong = new AtomicLong(0);
    @Override
    public void run(String... args) {
        log.info("PlumeLogApplication服务器已启动完毕");
        for (int i = 0; i < 10; i++) {
            int finalI = i;
            new Thread(() -> {
                while (true) {
                    log.info("我是日志" + finalI + ".我是第" + atomicLong.addAndGet(1) + "条");
                }
            }).start();
        }
    }
}
