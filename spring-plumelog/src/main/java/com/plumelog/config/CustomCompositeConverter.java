package com.plumelog.config;


import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.pattern.CompositeConverter;
import com.plumelog.core.TraceId;
import org.springframework.boot.ansi.AnsiOutput;

/**
 * Logback {@link CompositeConverter} colors output using the {@link AnsiOutput} class. A
 * single 'color' option can be provided to the converter, or if not specified color will
 * be picked based on the logging level.
 *
 * @author Phillip Webb
 * @since 1.0.0
 */
public class CustomCompositeConverter extends CompositeConverter<ILoggingEvent> {

    @Override
    protected String transform(ILoggingEvent event, String in) {
        if (TraceId.logTraceID.get() != null) {
            return TraceId.logTraceID.get();
        } else {
            return "";
        }
    }

}

