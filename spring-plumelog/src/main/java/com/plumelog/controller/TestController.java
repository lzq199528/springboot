package com.plumelog.controller;

import com.alibaba.ttl.threadpool.TtlExecutors;
import com.plumelog.impl.TestImpl;
import com.plumelog.trace.annotation.Trace;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.*;

/**
 * @author liuzhiqiang
 */
@RestController
@CrossOrigin
@Slf4j
public class TestController {

    @Autowired
    private TestImpl testImpl;

    private static ExecutorService executorService = TtlExecutors.getTtlExecutorService(
            new ThreadPoolExecutor(8, 8, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>()));

    private static ThreadPoolExecutor executorService1 = new ThreadPoolExecutor(8, 8, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
    @GetMapping("test")
    @Trace
    public String test() {
        log.info("ddd");
        log.info("fff");
        new Thread(() -> testImpl.logPrint()).start();

        executorService.execute(() -> {
            log.info("线程池");
            throw new RuntimeException("线程池异常");
        });
        testImpl.asyncTest();
        executorService1.execute(() -> {
            try {
                log.info("线程池1");
                throw new RuntimeException("线程池1异常");
            } catch (Exception e) {
                log.error("异常",e);
            }

        });
        return "ddd";
    }


}
