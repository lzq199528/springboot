package com.spring.es.domain;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * @author liuzhiqiang
 */
@Document(indexName = "user")
@Data
public class UserDocument {

    private String userName;

    private String password;
}
