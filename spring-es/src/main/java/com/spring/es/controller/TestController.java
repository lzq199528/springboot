package com.spring.es.controller;

import com.spring.es.domain.UserDocument;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.StringQuery;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuzhiqiang
 */
@RestController
@Slf4j
public class TestController {

    @Autowired(required = false)
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @GetMapping("/test")
    public String test() {
//        for (int i = 0; i < 100; i++) {
//            UserDocument userDocument = new UserDocument();
//            userDocument.setUserName("liuzhiqiang");
//            userDocument.setPassword(String.valueOf(i));
//            elasticsearchRestTemplate.save(userDocument);
//        }

//        通过match实现全文搜索
//        "match": {
//            "{FIELD}": "{TEXT}"
//        }

//        通过term实现精确匹配语法
//        "term": {
//            "{FIELD}": "{VALUE}"
//        }
//        通过terms实现SQL的in语句
//        "terms": {
//            "{FIELD}": ["{VALUE1}", "{VALUE2}"]
//        }
//        通过range实现范围查询，类似SQL语句中的>, >=, <, <=表达式。
//        "range": {
//            "{FIELD}": { "gte": 10, "lte": 20 }
//        }
//        gte范围参数 - 等价于>=
//        lte范围参数 - 等价于 <=
//        gt - 大于 （ > ）
//        gte - 大于且等于 （ >= ）
//        lt - 小于 （ < ）
//        lte - 小于且等于 （ <= ）
//        在ES中bool查询就是用来组合布尔查询条件，布尔查询条件，就是类似SQL中的and （且）、or （或）。
//        "bool": { // bool查询
//            "must": [], // must条件，类似SQL中的and, 代表必须匹配条件
//            "must_not": [], // must_not条件，跟must相反，必须不匹配条件
//            "should": [] // should条件，类似SQL中or, 代表匹配其中一个条件
//        }
//        示例
//        "bool": {
//            "should": [
//            {
//                "bool": {
//                "must": [
//                {
//                    "term": {
//                    "order_no": "2020031312091209991"
//                }
//                },
//                {
//                    "range": {
//                    "shop_id": {
//                        "gte": 10,
//                         "lte": 200
//                    }
//                }
//                }
//            ]
//            }
//            },
//            {
//                "terms": {
//                "tag": [
//                1,
//                        2,
//                        3,
//                        4,
//                        5,
//                        12
//            ]
//            }
//            }
//      ]
//        }
//        select * from order_v2 where (order_no='202003131209120999' and (shop_id>=10 and shop_id<=200)) or tag in (1,2,3,4,5)

        String jsonQuery = "{\"match\":{\"userName\":\"liuzhiqiang\"}}";
        StringQuery stringQuery = new StringQuery(jsonQuery);
        stringQuery.setPageable(PageRequest.of(10, 20));
        SearchHits<UserDocument> userDocumentList = elasticsearchRestTemplate.search(stringQuery,UserDocument.class);
        for (SearchHit<UserDocument> userDocumentSearchHit : userDocumentList) {
           log.info("数据{}",userDocumentSearchHit.getContent());
           log.info("id:{},index:{}", userDocumentSearchHit.getId(),userDocumentSearchHit.getIndex());
        }
        return "成功";
    }
}
