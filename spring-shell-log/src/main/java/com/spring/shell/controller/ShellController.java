package com.spring.shell.controller;

import com.sun.javafx.util.Utils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liuzhiqiang
 */
@RestController
@RequestMapping("/")
public class ShellController {

    @GetMapping("/shell")
    public String test(String cmd) {
        List<String> result = new ArrayList<>();
        try {
            Process ps = Runtime.getRuntime().exec(cmd);
            int exitValue = ps.waitFor();
            if (0 != exitValue) {
                System.out.println("call shell failed. error code is :" + exitValue);
            }
            // 只能接收脚本echo打印的数据，并且是echo打印的最后一次数据
            BufferedInputStream in = new BufferedInputStream(ps.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(in, "GBK"));
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println("脚本返回的数据如下： " + line);
                result.add(line);
            }
            in.close();
            br.close();


        } catch (Exception e) {
            e.printStackTrace();
        }

        return String.join("\n", result);
    }

    @GetMapping("/shellLog")
    public String test(Integer lines,String keyword) {
        List<String> result = new ArrayList<>();
        List<Process> processList = new ArrayList<>();
        try {
            Process ps = null;
            if (Utils.isWindows()) {
                StringBuilder cmd = new StringBuilder(" Get-Content ");
                if (lines != null) {
                    cmd.append(" -Tail " + lines);
                }
                cmd.append(" -Encoding UTF8 ");
                cmd.append(" log/fileLog.log ");
                if (keyword != null && !keyword.equals("")) {
                    cmd.append(" | ");
                    cmd.append(" findstr /I /C:");
                    cmd.append("'" +keyword + "'");
                }
                ps = Runtime.getRuntime().exec(new String[]{"powershell", "/c", cmd.toString()});
            } else if (Utils.isUnix()) {
                ps = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "tail -n " + lines + "log/fileLog.log"});
            } else if (Utils.isMac()) {

            } else if (Utils.isQVGAScreen()) {

            }
            if (ps != null) {
                int exitValue = ps.waitFor();
                if (0 != exitValue) {
                    System.out.println("call shell failed. error code is :" + exitValue);
                }
                // 只能接收脚本echo打印的数据，并且是echo打印的最后一次数据
                BufferedInputStream in = new BufferedInputStream(ps.getInputStream());
                BufferedReader br = new BufferedReader(new InputStreamReader(in, "GBK"));
                String line;
                while ((line = br.readLine()) != null) {
                    System.out.println("脚本返回的数据如下： " + line);
                    result.add(line);
                }
                in.close();
                br.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return String.join("\n", result);
    }
}
