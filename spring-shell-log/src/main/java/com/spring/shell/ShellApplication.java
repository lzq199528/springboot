package com.spring.shell;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author liuzhiqiang
 */
@SpringBootApplication
@Slf4j
public class ShellApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ShellApplication.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("ShellApplication服务器已启动完毕");
    }
}
