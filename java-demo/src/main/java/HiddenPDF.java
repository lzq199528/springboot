import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class HiddenPDF {
    public static void main(String[] args) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("hidden.pdf"));
            document.open();
            document.add(new Paragraph("这是一个隐藏信息的PDF文件。"));

            // 添加隐藏字段
            document.addTitle("隐藏标题");
            document.addAuthor("隐藏作者");
            document.addSubject("隐藏主题");
            document.addHeader("xxx","ccc");
            document.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
