package com.spring.redisson;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

/**
 * 开发公司：联信
 * 版权：联信
 * <p>
 * Annotation
 * 分布式锁
 *
 * @author 刘志强
 * @created Create Time: 2021/5/10
 */
public class MyRedissonConfig {

    private static String host = "101.37.152.195";
    private static String port = "6379";
    private static String password = "";
    private static Integer database = 1;

    private static RedissonClient redissonClient;

    public static RedissonClient redisson() {
        if (redissonClient == null) {
            Config config = new Config();
            config.useSingleServer()
                    .setAddress("redis://" + host + ":" + port)
//                    .setPassword(password)
                    .setDatabase(database)
                    .setTimeout(1000)
                    .setPingConnectionInterval(0);
            redissonClient = Redisson.create(config);
        }
        return redissonClient;
    }
}
