package com.liuzhiqiang.shiro.vo;


import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * 操作消息提醒
 *
 * @author
 */
@Data
public class AjaxResult<T> {
    /**
     * 响应状态
     */
    private Integer code;

    /**
     * 响应描述
     */
    private String msg;

    /**
     * 响应数据
     */
    private T data;

    private Boolean success;

    public AjaxResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
        if (code == HttpStatus.OK.value()) {
            this.success = true;
        } else {
            this.success = false;
        }
    }

    public AjaxResult(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        if (code == HttpStatus.OK.value()) {
            this.success = true;
        } else {
            this.success = false;
        }
    }

    public static <T> AjaxResult<T> success() {
        return AjaxResult.success("success");
    }

    public static <T> AjaxResult<T> success(String msg) {
        return new AjaxResult<>(HttpStatus.OK.value(), msg);
    }

    public static <T> AjaxResult<T> success(String msg, T data) {
        return new AjaxResult<>(HttpStatus.OK.value(), msg, data);
    }

    public static <T> AjaxResult<T> successData(T data) {
        return new AjaxResult<>(HttpStatus.OK.value(), "success", data);
    }

    public static <T> AjaxResult<T> error(String msg) {
        return new AjaxResult<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), msg);
    }

    public static <T> AjaxResult<T> error(Integer code, String msg) {
        return new AjaxResult<>(code, msg);
    }
}