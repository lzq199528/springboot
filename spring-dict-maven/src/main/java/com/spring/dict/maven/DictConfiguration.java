package com.spring.dict.maven;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author liuzhiqiang
 */
@Configuration
public class DictConfiguration implements ImportSelector {

    public static final String SUFFIX = "Describe";

    public static String[] packagePath;

    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        Object packagePath = importingClassMetadata.getAnnotationAttributes("com.spring.dict.maven.EnableDict").get("packagePath");
        if (packagePath != null) {
            DictConfiguration.packagePath = (String[]) packagePath;
        }
        return new String[]{DictAopAspect.class.getName()};
    }
}
