package com.spring.dict.maven;

/**
 * 返回值
 *
 * @author sunyu
 * @date 2021/08/05 14:23
 **/
public interface Result<T> {
    /**
     * 获取数据
     *
     * @return
     */
    T getData();

    /**
     * 设置数据
     *
     * @param o
     */
    void setData(T o);
}
