package com.spring.dict.maven;

import java.lang.annotation.*;

/**
 * @author liuzhiqiang
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Dict {

    /**
     * 转换后的字段 默认 原字段
     *
     * @return
     */
    String value() default "";

    /**
     * 转换后字段的后缀
     *
     * @return
     */
    String suffix() default "";

    /**
     * 类型
     *
     * @return
     */
    String type() default "";
}
