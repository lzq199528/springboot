package com.spring.dict.maven;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @author liuzhiqiang
 */
@Component
@Slf4j
public class DictUtil {

    @Resource
    private DictService service;

    private static DictService dictService;

    public static String getDict(String[] args, Object value) {
        try {
            return DictUtil.dictService.getDescribe(args, value);
        } catch (Exception e) {
            log.error(e.getMessage());
            return "";
        }

    }

    @PostConstruct
    public void init() {
        DictUtil.dictService = this.service;
    }
}
