package com.spring.dict.maven;

import java.lang.reflect.Field;

/**
 * @author liuzhiqiang
 */
public interface DictService {


    /**
     * 根据数据字典值和类型获取数据字典标签
     *
     * @param value
     * @param type
     * @return
     */
    default String getDictLabel(String value, String type) {
        return "";
    }


    /**
     * 空处理
     *
     * @param field
     * @return
     */
    default Object nullHandle(Field field) {
        return null;
    }


    /**
     * 获取描述
     * @param args
     * @return
     */
    default String getDescribe(String[] args, Object value) {
        return "";
    }
}
