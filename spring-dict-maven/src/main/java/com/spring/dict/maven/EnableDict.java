package com.spring.dict.maven;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author liuzhiqiang
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(DictConfiguration.class)
public @interface EnableDict {

    String[] packagePath() default {};
}
