package generator;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.fill.Column;
import org.apache.ibatis.annotations.Mapper;

import java.sql.Types;
import java.util.Collections;

/**
 * 代码生成
 *
 * @author liuzhiqiang
 */
public class CodeGenerator {
    public static void main(String[] args) {

        String serviceName = "spring-mybatis\\sql-conversion-plus";

        String moduleName = "";

        String tableName = "script_car_new";


        String rootDir = System.getProperty("user.dir");


        String servicePath = rootDir + "\\" + serviceName + "\\src\\main\\java";

        String xmlPath = rootDir + "\\" + serviceName + "\\src\\main\\resources\\mapper";


        FastAutoGenerator.create("jdbc:mysql://47.92.211.171:3308/script_murder?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC", "root", "root")
                .globalConfig(builder -> {
                    builder
                            .disableOpenDir()
                            .author("刘志强") // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .outputDir(servicePath); // 指定输出目录
                })
                .packageConfig(builder -> builder
                        .parent("com.mybatis.plus")
                        .moduleName(moduleName)
                        .entity("domain.entity")
                        .pathInfo(Collections.singletonMap(OutputFile.xml, xmlPath)))
                .templateConfig(builder -> {
                    // builder.disable(TemplateType.CONTROLLER);
                })
                .strategyConfig(builder -> {
                            builder.addInclude(tableName);
                            builder.entityBuilder()
                                    .enableLombok()
                                    .logicDeleteColumnName("del_flag")
                                    .addTableFills(
                                            new Column("created_date", FieldFill.INSERT),
                                            new Column("created_by", FieldFill.INSERT),
                                            new Column("update_date", FieldFill.INSERT_UPDATE),
                                            new Column("update_by", FieldFill.INSERT_UPDATE)
                                    )
                                    .enableFileOverride();
                            builder.serviceBuilder().convertServiceFileName(entityName -> entityName + "Service");
                            builder.mapperBuilder().mapperAnnotation(Mapper.class);
                            builder.controllerBuilder().enableRestStyle()
                                    .enableHyphenStyle()
                                    .enableFileOverride();
                        }
                )
                .dataSourceConfig(builder -> builder.typeConvertHandler((globalConfig, typeRegistry, metaInfo) -> {
                    int typeCode = metaInfo.getJdbcType().TYPE_CODE;
                    // 小数位长度
                    int scale = metaInfo.getScale();
                    // 长度
                    int length = metaInfo.getLength();
                    // 整数位长度
                    int integerBitLength = length - scale;

                    metaInfo.getScale();
                    if (typeCode == Types.NUMERIC) {
                        if ((scale == 0 && integerBitLength > 10) || length == 0) {
                            return DbColumnType.LONG;
                        } else if (scale == 0 && integerBitLength <= 10) {
                            return DbColumnType.INTEGER;
                        }
                    }
                    if (typeCode == Types.TINYINT) {
                        return DbColumnType.INTEGER;
                    }
                    return typeRegistry.getColumnType(metaInfo);
                }))
                .execute();
    }
}
