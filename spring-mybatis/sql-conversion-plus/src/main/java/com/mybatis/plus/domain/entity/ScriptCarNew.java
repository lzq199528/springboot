package com.mybatis.plus.domain.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 刘志强
 * @since 2024-06-14
 */
@Getter
@Setter
@TableName("script_car_new")
@ApiModel(value = "ScriptCarNew对象", description = "")
public class ScriptCarNew implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
      private Long id;

    @ApiModelProperty("商家ID")
    private Long storeId;

    @ApiModelProperty("剧本id")
    private Long scriptId;

    @ApiModelProperty("开本时间")
    private LocalDateTime startTime;

    @ApiModelProperty("男性玩家数量")
    private Integer manNumber;

    @ApiModelProperty("女性玩家数量")
    private Integer womanNumber;

    @ApiModelProperty("价格")
    private BigDecimal price;

    @ApiModelProperty("DMID")
    private Long dmId;

    @ApiModelProperty("店长推荐语")
    private String recommendedDescription;

    @ApiModelProperty("删除标志 0 未删除 1 删除")
    @TableLogic
    private Integer delFlag;

    @ApiModelProperty("创建时间")
      @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createdDate;

    @ApiModelProperty("创建人")
      @TableField(fill = FieldFill.INSERT)
    private Long createdBy;

    @ApiModelProperty("修改时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateDate;

    @ApiModelProperty("修改人")
      @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateBy;
}
