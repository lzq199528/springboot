package com.mybatis.plus.controller;

import com.mybatis.plus.service.ScriptCarNewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 刘志强
 * @since 2024-06-14
 */
@RestController
@CrossOrigin
@Slf4j
@Api(tags = "ScriptCarNewController")
public class ScriptCarNewController {

    @Autowired
    private ScriptCarNewService scriptCarNewService;

    @PostMapping("/sql_conversion")
    @ApiOperation(value = "sql转换并执行")
    @ApiImplicitParam(name = "sql", value = "sql", dataType = "String", paramType = "query")
    public Object sqlConversion(String sql) {
        return scriptCarNewService.sqlConversion(sql);
    }

}
