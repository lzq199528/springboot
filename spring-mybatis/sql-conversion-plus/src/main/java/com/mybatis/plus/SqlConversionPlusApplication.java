package com.mybatis.plus;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author liuzhiqiang
 */
@SpringBootApplication(scanBasePackages = {"com.mybatis.plus"})
@MapperScan("com.mybatis.plus.mapper")
@Slf4j
public class SqlConversionPlusApplication {
    public static void main(String[] args) {
        SpringApplication.run(SqlConversionPlusApplication.class, args);
        log.info("SqlConversionPlusApplication 服务已启动");
    }
}
