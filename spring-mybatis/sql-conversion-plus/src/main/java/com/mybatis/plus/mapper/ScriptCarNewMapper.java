package com.mybatis.plus.mapper;

import com.mybatis.plus.domain.entity.ScriptCarNew;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 刘志强
 * @since 2024-06-14
 */
@Mapper
public interface ScriptCarNewMapper extends BaseMapper<ScriptCarNew> {

    /**
     * sqlConversion
     * @param sql
     * @return
     */
    Object sqlConversion(@Param("sql") String sql);
}
