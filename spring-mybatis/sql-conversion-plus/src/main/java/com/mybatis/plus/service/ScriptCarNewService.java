package com.mybatis.plus.service;

import com.mybatis.plus.domain.entity.ScriptCarNew;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 刘志强
 * @since 2024-06-14
 */
public interface ScriptCarNewService extends IService<ScriptCarNew> {

    /**
     * sql转换并执行
     * @param sql
     * @return
     */
    Object sqlConversion(String sql);
}
