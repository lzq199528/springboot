package com.mybatis.plus.service.impl;

import com.mybatis.plus.domain.entity.ScriptCarNew;
import com.mybatis.plus.mapper.ScriptCarNewMapper;
import com.mybatis.plus.service.ScriptCarNewService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 刘志强
 * @since 2024-06-14
 */
@Service
public class ScriptCarNewServiceImpl extends ServiceImpl<ScriptCarNewMapper, ScriptCarNew> implements ScriptCarNewService {

    @Autowired
    private ScriptCarNewMapper scriptCarNewMapper;

    @Override
    public Object sqlConversion(String sql) {
        Object obj = scriptCarNewMapper.sqlConversion(sql);
        return obj;
    }
}
