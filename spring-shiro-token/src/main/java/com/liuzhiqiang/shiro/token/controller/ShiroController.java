package com.liuzhiqiang.shiro.token.controller;

import com.liuzhiqiang.shiro.token.vo.AjaxResult;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShiroController {

    @GetMapping("test")
    public AjaxResult test() {
        return AjaxResult.successData("测试无需认证");
    }

    @GetMapping("test1")
    @RequiresPermissions("shiro:all")
    public AjaxResult test1() {
        return AjaxResult.successData("测试有权限");
    }

    @GetMapping("test2")
    @RequiresPermissions("shiro:xxx-xxx")
    public AjaxResult test2() {
        return AjaxResult.successData("测试无权限");
    }
}
