import org.flowable.engine.ProcessEngine;
import org.flowable.engine.ProcessEngineConfiguration;
import org.flowable.engine.impl.cfg.StandaloneProcessEngineConfiguration;

public class TestMain {
    public static void main(String[] args) {
        StandaloneProcessEngineConfiguration cfg = new StandaloneProcessEngineConfiguration();
        cfg.setJdbcUrl("jdbc:mysql://101.37.152.195:3306/flowable?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC&nullCatalogMeansCurrent=true");
        cfg.setJdbcUsername("root");
        cfg.setJdbcPassword("lzq199528");
        cfg.setJdbcDriver("com.mysql.cj.jdbc.Driver");
        cfg.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);//设置了true，确保在JDBC参数连接的数据库中，数据库表结构不存在时，会创建相应的表结构。
        ProcessEngine processEngine = cfg.buildProcessEngine();
    }
}
