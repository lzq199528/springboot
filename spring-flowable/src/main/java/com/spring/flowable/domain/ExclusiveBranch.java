package com.spring.flowable.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liuzhiqiang
 */
@Data
public class ExclusiveBranch {

    @ApiModelProperty(value = "id")
    private String id;
    @ApiModelProperty(value = "分支条件")
    private String condition;
    @ApiModelProperty(value = "分支内部流程")
    private ProcessNode process;
}
