package com.spring.flowable.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liuzhiqiang
 */
@Data
public class ProcessNode {

    @ApiModelProperty(value = "节点ID")
    private String id;

    @ApiModelProperty(value = "节点名称")
    private String name;

    @ApiModelProperty(value = "节点类型 AUDIT_NODE(审核节点) BRANCH_NODE(分支节点)")
    private String type;

    @ApiModelProperty(value = "下一节点")
    private ProcessNode next;

    @ApiModelProperty(value = "分支")
    private List<ExclusiveBranch> exclusive;

    @ApiModelProperty(value = "委托人")
    private Assignee assignee;

    @ApiModelProperty(value = "表单权限")
    private List<FormPerm> formPerms;
}
