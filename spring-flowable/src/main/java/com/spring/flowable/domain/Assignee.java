package com.spring.flowable.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liuzhiqiang
 */
@Data
public class Assignee {
    @ApiModelProperty(value = "委托人列表")
    private List<String> users;
    @ApiModelProperty(value = "是否顺序审批")
    private Boolean sequential;

    public Boolean getSequential() {
        if (sequential == null) {
            return false;
        }
        return sequential;
    }
}
