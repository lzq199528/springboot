package com.spring.flowable.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author liuzhiqiang
 */
@Data
@AllArgsConstructor
public class Inout {
    private String in;
    private String out;
}
