package com.spring.flowable.config;

import org.flowable.engine.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author liuzhiqiang
 */
@Configuration
public class FlowableConfing {

    @Autowired
    private ProcessEngine processEngine;

    @Bean
    @ConditionalOnMissingBean
    public RuntimeService runtimeService() {
        return processEngine.getRuntimeService();
    }
    @Bean
    @ConditionalOnMissingBean
    public HistoryService historyService() {
        return processEngine.getHistoryService();
    }
    @Bean
    @ConditionalOnMissingBean
    public TaskService taskService() {
        return processEngine.getTaskService();
    }

    @Bean
    @ConditionalOnMissingBean
    public FormService formService() {
        return processEngine.getFormService();
    }

    @Bean
    @ConditionalOnMissingBean
    public RepositoryService repositoryService() {
        return processEngine.getRepositoryService();
    }

    @Bean
    @ConditionalOnMissingBean
    public IdentityService identityService() {
        return processEngine.getIdentityService();
    }

    @Bean
    @ConditionalOnMissingBean
    public DynamicBpmnService dynamicBpmnService() {
        return processEngine.getDynamicBpmnService();
    }

    @Bean
    @ConditionalOnMissingBean
    public ManagementService managementService() {
        return processEngine.getManagementService();
    }

    @Bean
    @ConditionalOnMissingBean
    public ProcessMigrationService processMigrationService() {
        return processEngine.getProcessMigrationService();
    }
}
