package com.spring.flowable.util;

import cn.hutool.core.util.StrUtil;
import lombok.Getter;
import org.flowable.bpmn.model.ExclusiveGateway;
import org.flowable.bpmn.model.ParallelGateway;
import org.flowable.bpmn.model.ServiceTask;
import org.flowable.bpmn.model.UserTask;

/**
 * 节点类型
 *
 * @author liuzhiqiang
 */
@Getter
public enum TypeEnum {
    /**
     * 发起人
     */
    ROOT("ROOT", UserTask.class),
    /**
     * 审批
     */
    APPROVAL ("APPROVAL", UserTask.class),
    /**
     * 抄送节点
     */
    CC("CC", ServiceTask.class),
    /**
     * 条件(排他网关)
     */
    ROUTER("ROUTER", ExclusiveGateway.class),
    /**
     * 路由(并行网关)
     */
    PARALLEL("PARALLEL", ParallelGateway.class),
    /**
     * 触发器
     */
    TRIGGER("TRIGGER", ServiceTask.class),
    ;

    private String type;

    private Class targetFlowNode;


    TypeEnum(String type, Class targetFlowNode) {
        this.type = type;
        this.targetFlowNode = targetFlowNode;
    }

    public static TypeEnum getTypeEnumByType(String type) {
        for (TypeEnum value : TypeEnum.values()) {
            if (StrUtil.equals(type, value.type)) {
                return value;
            }
        }
        return null;
    }
}
