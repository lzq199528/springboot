package com.redis;

import cn.hutool.json.JSONObject;
import com.wecom.WeComService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

/**
 * @author liuzhiqiang
 */
@SpringBootApplication(scanBasePackages = "com")
@Slf4j
public class RedisApplication implements CommandLineRunner {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private WeComService weComService;


    public static void main(String[] args) {
        SpringApplication.run(RedisApplication.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("RedisApplication服务器已启动完毕");
        List<JSONObject> deptList = weComService.weComDeptList();
        log.info("获取部门成功");
//        new Thread(() -> {
//            while (true) {
//                try {
//                    String event = stringRedisTemplate.opsForList().leftPop("CallCenter_FS", Duration.ofSeconds(30));
//                    if (StringUtils.isNotBlank(event)) {
//                        log.info("event: {}", event);
//                        continue;
//                    } else {
//                        log.info("event:{}", "空");
//                    }
//                } catch (Exception e) {
//                    log.error("",e);
//                }
//
//            }
//        }).start();
//        new Thread(() -> {
//            while (true) {
//                stringRedisTemplate.opsForList().leftPush("CallCenter_FS", "222222222");
//                try {
//                    Thread.sleep(100);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();

    }
}
