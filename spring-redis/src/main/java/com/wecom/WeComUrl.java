package com.wecom;

public class WeComUrl {

    /**
     * 获取TOKEN
     */
    public static final String GET_TOKEN_URL = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={corpid}&corpsecret={secret}";

    /**
     * 获取企业微信部门id列表接口
     */
    public static final String DEPARTMENT_LIST_URL = "https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token={accessToken}";



}
