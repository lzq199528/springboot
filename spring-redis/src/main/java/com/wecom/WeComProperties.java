package com.wecom;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "wecom")
@Data
public class WeComProperties {

    private String corpid;

    private String addressbookSecret;

    private String crmSecret;
}
