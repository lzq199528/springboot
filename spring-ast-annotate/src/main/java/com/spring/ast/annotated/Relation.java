package com.spring.ast.annotated;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author liuzhiqiang
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.SOURCE)
public @interface Relation {

    /**
     * 目标表
     *
     * @return
     */
    String table();

    /**
     * 展示字段
     *
     * @return
     */
    String showField();

    /**
     * 目标字段
     *
     * @return
     */
    String targetField() default "id";

    /**
     * 其他参数
     *
     * @return
     */
    String[] args() default {};

    /**
     * 别名
     * @return
     */
    String alias() default "";
}
