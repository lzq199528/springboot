package com.spring.ast.annotated;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author liuzhiqiang
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.SOURCE)
public @interface Describe {

    /**
     * 类型
     *
     * @return
     */
    String type() default "";

    /**
     * 其他参数
     *
     * @return
     */
    String[] args() default {};

    /**
     * 别名
     * @return
     */
    String alias() default "";

}
