package com.spring.ast.service;

import com.spring.ast.annotated.Describe;
import com.spring.ast.annotated.Relation;

/**
 * @author liuzhiqiang
 */
public interface ExchangeService {

    /**
     * 关联关系处理
     * @param relation
     * @param value
     * @return
     */
    String relationHandle(Relation relation, Object value);

    /**
     * 描述字段处理
     * @param describe
     * @param value
     * @return
     */
    String describeHandle(Describe describe, Object value);
}
