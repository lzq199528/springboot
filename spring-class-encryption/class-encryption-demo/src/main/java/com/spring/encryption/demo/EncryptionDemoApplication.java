package com.spring.encryption.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author liuzhiqiang
 */
@SpringBootApplication
@Slf4j
public class EncryptionDemoApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(EncryptionDemoApplication.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("EncryptionDemoApplication服务器已启动完毕");
    }


}
