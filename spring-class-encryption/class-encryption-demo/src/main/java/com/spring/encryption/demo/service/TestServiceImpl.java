package com.spring.encryption.demo.service;

import org.springframework.stereotype.Component;

/**
 * @author liuzhiqiang
 */
@Component
public class TestServiceImpl {

    public String getTest() {
        return "getTest";
    }
}
