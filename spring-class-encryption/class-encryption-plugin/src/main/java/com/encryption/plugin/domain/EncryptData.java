package com.encryption.plugin.domain;

import java.io.Serializable;
import java.util.Arrays;

/**
 * @author liuzhiqiang
 */
public class EncryptData implements Serializable {

    private String[] packages;

    private String password;

    private String[] suffix;

    private String[] files;

    public String[] getPackages() {
        return packages;
    }

    public void setPackages(String[] packages) {
        this.packages = packages;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String[] getSuffix() {
        return suffix;
    }

    public void setSuffix(String[] suffix) {
        this.suffix = suffix;
    }

    public String[] getFiles() {
        return files;
    }

    public void setFiles(String[] files) {
        this.files = files;
    }

    @Override
    public String toString() {
        return "EncryptData{" +
                "packages=" + Arrays.toString(packages) +
                ", password='" + password + '\'' +
                ", suffix=" + Arrays.toString(suffix) +
                ", files=" + Arrays.toString(files) +
                '}';
    }
}
