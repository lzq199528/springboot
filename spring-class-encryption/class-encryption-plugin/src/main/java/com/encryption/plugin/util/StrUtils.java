package com.encryption.plugin.util;

import java.util.List;
import java.util.regex.Pattern;

/**
 * 字符串工具
 *
 * @author roseboy
 */
public class StrUtils {

    /**
     * 判断字符串是否为空
     *
     * @param str 字符串
     * @return 是否是空的
     */
    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    public static boolean isMatchs(List<String> matches, String testString, boolean dv) {
        if (matches == null || matches.size() == 0) {
            return dv;
        }

        for (String m : matches) {
            if (StrUtils.isMatch(m, testString) || testString.startsWith(m) || testString.endsWith(m)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 通配符匹配
     *
     * @param match      匹配字符串
     * @param testString 待匹配字符窜
     * @return 是否匹配
     */
    public static boolean isMatch(String match, String testString) {
        String regex = match.replaceAll("\\?", "(.?)")
                .replaceAll("\\*+", "(.*?)");
        return Pattern.matches(regex, testString);
    }
}
