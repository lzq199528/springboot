package com.encryption.plugin;

import com.encryption.plugin.util.AgentTransformer;

import java.lang.instrument.Instrumentation;

/**
 * 解密程序
 *
 * @author liuzhiqiang
 */
public class BeforeLoadAgent {


    public static void premain(String agentArgs, Instrumentation inst) {
        if (inst != null) {
            AgentTransformer tran = new AgentTransformer();
            inst.addTransformer(tran);
        }
    }
}
