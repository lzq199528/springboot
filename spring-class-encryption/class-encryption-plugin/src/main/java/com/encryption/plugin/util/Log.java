package com.encryption.plugin.util;

import java.util.logging.Logger;

/**
 * @author liuzhiqiang
 */
public class Log {
    public static Logger logger = Logger.getLogger("log");

    public static void info(String msg) {
        logger.info(msg);
    }

}
