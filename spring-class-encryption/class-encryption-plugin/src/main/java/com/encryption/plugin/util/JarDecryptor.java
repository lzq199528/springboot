package com.encryption.plugin.util;


import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * java class解密
 *
 * @author roseboy
 */
public class JarDecryptor {

    private static final JarDecryptor single = new JarDecryptor();

    public static JarDecryptor getInstance() {
        return single;
    }

    /**
     * 解密配置文件，spring读取文件时调用
     *
     * @param path 配置文件路径
     * @param in   输入流
     * @return 解密的输入流
     */
    public InputStream decryptConfigFile(String path, InputStream in, String pass) {
        if (path.endsWith(".class")) {
            return in;
        }
        String projectPath = JarUtils.getRootPath(null);
        if (StrUtils.isEmpty(projectPath)) {
            return in;
        }
        byte[] bytes = null;
        try {
            bytes = IoUtils.toBytes(in);
        } catch (Exception e) {

        }
        try {
            bytes = DESUtil.decryptByte(pass, bytes);
            Log.info("解密文件[" + path + "]");
        } catch (Exception e) {
            return in;
        }
        if (bytes == null) {
            return in;
        }
        in = new ByteArrayInputStream(bytes);
        return in;
    }
}
