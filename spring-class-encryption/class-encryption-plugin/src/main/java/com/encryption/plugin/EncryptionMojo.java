package com.encryption.plugin;

import com.encryption.plugin.domain.EncryptData;
import com.encryption.plugin.util.JarUtils;
import com.encryption.plugin.util.Log;
import org.apache.maven.model.Build;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.File;

/**
 * 加密插件入口
 *
 * @author liuzhiqiang
 */
@Mojo(name = "encryption", defaultPhase = LifecyclePhase.PACKAGE)
public class EncryptionMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    private MavenProject project;

    @Parameter
    private String[] packages;

    @Parameter
    private String password;

    @Parameter(defaultValue = "class", readonly = true, required = true)
    private String[] suffix;

    @Parameter
    private String[] files;

    /**
     * 执行加密过程
     */
    @Override
    public void execute() {

        EncryptData encryptData = new EncryptData();
        encryptData.setPackages(packages);
        encryptData.setPassword(password);
        encryptData.setSuffix(suffix);
        encryptData.setFiles(files);

        Build build = project.getBuild();
        String targetJar = build.getDirectory() + File.separator + build.getFinalName()
                + "." + project.getPackaging();
        Log.info("获得目标jar[" + targetJar + "]");
        JarUtils jarUtils = new JarUtils(encryptData, targetJar);
        try {
            Long startDate = System.currentTimeMillis();
            Log.info("加密处理开始[" + startDate + "]");
            jarUtils.encryptionJar();
            Long endDate = System.currentTimeMillis();
            Log.info("加密处理结束[" + endDate + "]");
            Log.info("用时[" + (endDate - startDate) / 1000 + "]秒");
        } catch (Exception e) {
            Log.info("加密处理失败");
            e.printStackTrace();
        }

    }
}
