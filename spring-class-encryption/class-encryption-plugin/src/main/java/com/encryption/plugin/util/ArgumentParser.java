package com.encryption.plugin.util;

/**
 * @author liuzhiqiang
 * 命令行参数解析
 */
public class ArgumentParser {

    public static String analysis(String agentArgs, String target) {
        target = "-" + target + " ";
        Integer start = agentArgs.indexOf(target);
        if (start != -1) {
            start = start + target.length();
            String targetResult = agentArgs.substring(start);
            Integer end = targetResult.indexOf(" ");
            if (end != -1) {
                targetResult = agentArgs.substring(start, end + start);
            }
            return targetResult;
        } else {
            return null;
        }

    }
}
