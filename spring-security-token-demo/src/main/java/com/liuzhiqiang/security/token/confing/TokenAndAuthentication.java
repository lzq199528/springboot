package com.liuzhiqiang.security.token.confing;

import org.springframework.security.core.Authentication;

import java.util.concurrent.ConcurrentHashMap;

public class TokenAndAuthentication {

    private static ConcurrentHashMap<String, Authentication> map = new ConcurrentHashMap();

    public static Authentication getAuthentication(String token) {
        return map.get(token);
    }

    public static void setTokenAndAuthentication(String token, Authentication authentication) {
        map.put(token, authentication);
    }

    public static void delete(String token) {
        map.remove(token);
    }
}
