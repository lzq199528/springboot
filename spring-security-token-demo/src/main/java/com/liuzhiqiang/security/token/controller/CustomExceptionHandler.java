package com.liuzhiqiang.security.token.controller;

import com.liuzhiqiang.security.token.vo.AjaxResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 刘志强
 * @title: ExceptionHandler
 * @projectName
 * @description: TODO
 * @date 2021/7/411:39
 */
@RestController
@ControllerAdvice
@Slf4j
public class CustomExceptionHandler {


    @ExceptionHandler(AccessDeniedException.class)
    @CrossOrigin
    public AjaxResult handlerAccessDeniedException(AccessDeniedException ex) {
        log.error(ex.getMessage());
        ex.printStackTrace();
        return AjaxResult.error(HttpStatus.UNAUTHORIZED.value(), "权限不足");
    }

    @ExceptionHandler(RuntimeException.class)
    @CrossOrigin
    public AjaxResult handlerRuntimeException(RuntimeException ex) {
        log.error(ex.getMessage());
        ex.printStackTrace();
        return AjaxResult.error(ex.getMessage());
    }
}
