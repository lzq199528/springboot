package com.liuzhiqiang.oauth.confing.oauth;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author 刘志强
 * @title: UserPermission
 * @projectName housekeeper-data-center-back
 * @description: TODO
 * @date 2021/7/2822:21
 */
@Data
public class UserPermission implements GrantedAuthority {

    private String authority;

    public UserPermission(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return this.authority;
    }
}
