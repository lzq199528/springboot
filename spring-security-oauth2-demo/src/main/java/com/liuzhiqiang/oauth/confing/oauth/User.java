package com.liuzhiqiang.oauth.confing.oauth;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @author 刘志强
 * @title: User
 * @projectName housekeeper-data-center-back
 * @description: TODO
 * @date 2021/7/2822:19
 */
@Data
public class User implements UserDetails, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名 密码
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 权限
     */
    private List<UserPermission> list;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.list;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
