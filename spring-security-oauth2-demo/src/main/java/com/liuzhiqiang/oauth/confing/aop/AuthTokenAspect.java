package com.liuzhiqiang.oauth.confing.aop;

import com.liuzhiqiang.oauth.vo.AjaxResult;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @author 刘志强
 * @title: AuthTokenAspect
 * @projectName housekeeper-data-center-back
 * @description: TODO
 * @date 2021/7/3122:59
 * 切入 /oauth/token方法，改变返回值
 */

@Aspect
@Component
@Slf4j
public class AuthTokenAspect {

    @Around("execution(* org.springframework.security.oauth2.provider.endpoint.TokenEndpoint.postAccessToken(..))")
    public Object handleControllerMethod(ProceedingJoinPoint pjp) throws Throwable {
        Object proceed = pjp.proceed();
        return ResponseEntity.ok(AjaxResult.successData(((ResponseEntity) proceed).getBody()));
    }

}
