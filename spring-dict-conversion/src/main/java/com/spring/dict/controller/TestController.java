package com.spring.dict.controller;

import com.spring.dict.domain.Result;
import com.spring.dict.domain.TestDomain;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author liuzhiqiang
 */
@RestController
@CrossOrigin
public class TestController {


    @GetMapping("/test")
    public Result test() {
        TestDomain testDomain = new TestDomain();
        testDomain.setName("测试");
        testDomain.setCustomer("fff");
        return Result.okData(testDomain);
    }

    @GetMapping("/testList")
    public Result<List<Map>> testList() {
        return Result.okData(null);
    }
}
