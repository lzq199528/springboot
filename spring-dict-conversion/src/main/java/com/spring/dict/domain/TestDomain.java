package com.spring.dict.domain;

import com.spring.ast.annotated.Describe;
import com.spring.ast.annotated.Relation;
import lombok.Data;

/**
 * @author liuzhiqiang
 */
@Data
public class TestDomain {

    @Describe(type = "city", alias = "nameCity")
    private String name;

    @Relation(table = "customer", showField = "customerName", alias = "customerName")
    private String customer;

}
