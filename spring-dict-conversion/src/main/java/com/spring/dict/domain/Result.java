package com.spring.dict.domain;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * 返回值
 *
 * @author sunyu
 * @date 2021/08/05 14:23
 **/
@Data
public class Result<T> {
    private static final long serialVersionUID = 1820006454966099648L;

    Integer code = HttpStatus.OK.value();

    boolean success = false;

    String message = "";

    T data;

    Long page = 0L;

    Long total = 0L;

    public static <T> Result<T> okData(T t) {
        Result<T> result = new Result();
        result.setCode(HttpStatus.OK.value());
        result.setSuccess(true);
        result.setData(t);
        return result;
    }
}
