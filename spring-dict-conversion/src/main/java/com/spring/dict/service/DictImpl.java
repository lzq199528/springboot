package com.spring.dict.service;

import com.spring.ast.annotated.Describe;
import com.spring.ast.annotated.Relation;
import com.spring.ast.service.ExchangeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author liuzhiqiang
 */
@Component
@Slf4j
public class DictImpl implements ExchangeService {


    @Override
    public String relationHandle(Relation relation, Object value) {
        log.info("select " + relation.showField() + " from " + relation.table() + " where " + relation.targetField() + " = '" + value + "'");
        return "xxx2";
    }

    @Override
    public String describeHandle(Describe describe, Object value) {
        log.info("select label from dict where type = {} and value = '{}'", describe.type(), value);
        return "xxx";
    }


}
