package com.spring.ip2region;

import cn.hutool.core.io.resource.ResourceUtil;
import org.lionsoul.ip2region.xdb.Searcher;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        // 1、创建 searcher 对象
        String dbPath = ResourceUtil.getResource("ip2region.xdb").getPath();
        Searcher searcher = null;
        try {
            searcher = Searcher.newWithFileOnly(dbPath);
        } catch (IOException e) {
            System.out.printf("failed to create searcher with `%s`: %s\n", dbPath, e);
            return;

        }

        // 2、查询
        try {
            String ip = "219.146.89.86";
            String region = searcher.search(ip);
            System.out.printf(region);
        } catch (Exception e) {
            System.out.printf("failed to search");
        }

        // 3、关闭资源
        searcher.close();
    }
}
