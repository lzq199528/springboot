package com.baidu;


import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author liuzhiqiang
 */
@SpringBootApplication
@Slf4j
public class BaiDuApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(BaiDuApplication.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("SaTokenApplication服务器已启动完毕");
    }
}
