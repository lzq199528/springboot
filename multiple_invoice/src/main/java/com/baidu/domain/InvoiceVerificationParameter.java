package com.baidu.domain;

import lombok.Data;

/**
 * @author liuzhiqiang
 */
@Data
public class InvoiceVerificationParameter {

    private String fileName;

    private String url;

    private String refId;
}
