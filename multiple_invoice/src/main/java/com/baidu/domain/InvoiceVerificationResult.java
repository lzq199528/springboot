package com.baidu.domain;

import cn.hutool.json.JSONObject;
import lombok.Data;

/**
 * @author liuzhiqiang
 */
@Data
public class InvoiceVerificationResult {

    /**
     * 结果 200 正常 500 异常
     */
    private Integer code;

    private String errorMessage;



    private String fileName;

    /**
     * 发票号
     */
    private String invoiceNum;

    private String invoiceDate;

    private String verifyResult;

    private String invalidSign;

    private String invoiceCode;

    private String invoiceType;

    private String wordsResultNum;

    private String checkCode;

    private String verifyMessage;

    private String logId;

    private String verifyFrequency;

    private String machineCode;

    /**
     * 元数据
     */
    private JSONObject metadata;
}
