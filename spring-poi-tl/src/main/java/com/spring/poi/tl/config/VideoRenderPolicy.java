package com.spring.poi.tl.config;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.util.StrUtil;
import com.deepoove.poi.data.PictureRenderData;
import com.deepoove.poi.data.PictureType;
import com.deepoove.poi.data.Pictures;
import com.deepoove.poi.data.style.PictureStyle;
import com.deepoove.poi.policy.AbstractRenderPolicy;
import com.deepoove.poi.render.RenderContext;
import com.deepoove.poi.xwpf.NiceXWPFDocument;
import com.spire.doc.Document;
import com.spire.doc.FileFormat;
import com.spire.doc.Section;
import com.spire.doc.documents.Paragraph;
import com.spire.doc.fields.DocPicture;
import org.apache.poi.ooxml.POIXMLDocument;
import org.apache.poi.ooxml.POIXMLDocumentPart;
import org.apache.poi.ooxml.POIXMLException;
import org.apache.poi.ooxml.POIXMLTypeLoader;
import org.apache.poi.ooxml.util.DocumentHelper;
import org.apache.poi.openxml4j.opc.*;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;
import org.xml.sax.InputSource;

import java.io.*;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author liuzhiqiang
 */
public class VideoRenderPolicy extends AbstractRenderPolicy<File> {

    private static final String ICON = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAYAAAB5fY51AAAAAXNSR0IArs4c6QAAGAdJREFUeF7tnQvQdVVZx/+ioRQg4IeXboiooKQgiEJqgBAXbwhBZoEaRN5SQAMvZNptSGumyRCTLjqpZY2EhiV28ZKGkI1M5iUvaFbeEkZKSo2S5p/7DOf93ts+Z+29nmft81sz7/Dpt9da//V71vv/1trnOWvdQRQIQAACjRC4QyM6kQkBCEBAGBaTAAIQaIYAhtVMqBAKAQhgWMwBCECgGQIYVjOhQigEIIBhMQcgAIFmCGBYzYQKoRCAAIbFHIAABJohgGE1EyqEQgACGBZzAAIQaIYAhtVMqBAKAQhgWMwBCECgGQIYVjOhQigEIIBhMQcgAIFmCGBYzYQKoRCAAIbFHIAABJohgGE1EyqEQgACGBZzAAIQaIYAhtVMqBAKAQhgWMwBCECgGQIYVjOhQigEIIBhMQcgAIFmCGBYzYQKoRCAAIbFHIAABJohgGE1EyqEQgACGBZzAAIQaIYAhtVMqBAKAQhgWMwBCECgGQIYVjOhQigEIIBhMQcgAIFmCGBYzYQKoRCAAIbFHIAABJohgGE1EyqEQgACGBZzAAIQaIYAhtVMqBAKAQhgWMwBCECgGQIYVjOhQigEIIBhMQcgAIFmCGBYzYQKoRCAAIbFHIAABJohgGE1EyqEQgACGBZzAAIQaIYAhtVMqBAKAQhgWMwBCECgGQIYVjOhQigEIIBhMQcgAIFmCGBYzYQKoRCAAIbFHIAABJohgGE1EyqEQgACGBZzAAIQaIYAhtVMqBAKAQhgWMwBCECgGQIYVjOhQigEIIBhMQcgAIFmCGBYzYQKoRCAAIbFHIAABJohgGE1EyqEQgACGBZzAAIQaIYAhtVMqBAKAQhgWMwBCECgGQIYVjOhQigEIIBhMQcgAIFmCGBYzYQKoRCAAIbFHIAABJohgGE1EyqEQgACGBZzAAIQaIYAhtVMqBAKAQiskmHtJmlfSTu6n9mf92IaTJ7ArZJu7H5u2unP/zP50U9ogFM2rDtJOlHSSd1/7zehuDGU4Qj8taQrJf2FpI8M1ywtjUFgaoZ1D0mnSTpO0vGS7joGNNqcLIEPdsb1LknvmOwoGx7YVAxrT0nPlPQsSd/bcDyQnofAVZIul/S2PJJQ0rphedtnk/LPgYQTAiMQwLhGgLpsky0b1pMlXSjpIcsOnnoQWICAjeuXJF23QB0eHZhAq4Zlo3rFwCxoDgLbEbhZ0gWSXrfdg/z9OARaNCy/Vzh3HBy0CoFeBC6R9OJeT/LQoARaMqx7Srpekv9LgUA0gTdKOjNaxKr134phPVTSB1YtOIw3PYG3S3pMepUTEtiCYd1d0pcmxJyhTIvAC3ifWi+gLRjWJyXdtx4SeoLAwgROl3TFwrWosDCB7Ib1J5Iev/CoqACB+gSOkfSe+t2uVo+ZDevlki5arXAw2oYJ3CDpiZI+3PAY0kvPalhHS3p3enoIhMBaAq+X9BSgjEcgq2G9VdITxhs2LUNgNAKet86Kp4xAIKNhObfF/1JRINAiAe8Mjm1Q+O6SDp/T/WlJ/5JtHNkM686SrpF0WDZQ6IHAAgSeLemyBZ6v/ajNyb9j8z8HbyDiUZLeV1vcVv1lM6wXSvLXHigQaJnApyR9v6QvJxrEPpJOnvvx/96uvFnSGds9VPPvsxnWxyQdVBMAfUFgJAI/JelVI7Xdt9l5k/LJu3frW3HuOZ8vl2ZrmMmw/C/S3ywBlCoQyEjAOYSnBAjz77QTWf3y3yuqZUxqXvYJ3SmsAUNZ32Umw7pY0i+moIIICJQT+Lokf63sq+VN9WrhwZ1R2awe0KtGv4fOk/TKfo+O/1Qmw7pW0sPHHzI9QKAagR+R9Icj9uYPqWxQ/nHS6hjl1d2JvmO0vXCbWQzL++TPLqyeChDITWCsRFKfXjIzqgNGRuALOR49ch+9m89iWP4Y+NLeqnkQAm0Q8B2IvgdziOLf1SdJ8qqt5rsxX332fUMMYIg2shiWl53PGGJAtAGBZATuJemLBZp8n6ZNyma1Ua5UQdO9qjo1w+/iUpQshuWD0PyxKwUCUyNwlCS/n120+GDA2Ypq10UrD/z8LpJuG7jNpZrLYljkXy0VPio1QMC3O72pp05fBDwzKRtdluJjyVMcopnFsL4m6S5ZooMOCAxIwN/e8FFJW5VHzG379h2w76GaelCWY3MyGJbd+wtDkaUdCCQj8JvdreQ7y/Lv3lO7FVX21yHHSXpnBq4ZDOtISe/PAAMNEBiBwNVdxvms6W+XdI6ksyUdOkJ/YzQ5dj5Zb80ZDMtHyzrXgwKBKRKYHTfjr8g8XdJzGryq7rmSfiNDcDCsDFG4XcOVkk7NJQk1hQRsWD422V+GbrX4K3MvySAew8oQhds1vKzL2XmepPvnkoaaFSbwmix5khhWrln4c5JsWvtLuqDbPuRSiJpVJOCV/2kZBo5hZYjC2hWWTWtWHtsZlz+loUAgioBPHfXpo+EFwwoPwRoBsxXW/P95R0nP74zLKSAUCNQm8AlJB9budKP+MKwMUbhdw0aGNfvbQzrTcu4OBQI1Cdwsae+aHW7WF4aVIQq3a/D7q/kt4Ubq/FUPv5T3ESMUCNQi4JX+N2t1hmFFk+7Xfx/Dckv+1262TXQiIgUCYxPYQ9ItY3eyXfussLYjVPfvt9oSbqTEL0L9aSK5W3XjtIq9+YiZ8FuAMKxcU29Rw5qpdwY1uVu5Yjk1NffOcCowhpVrWi1rWB7Fft020V/9oEBgaAK+2OIfh2500fYwrEWJjft8iWHNlJG7NW6MVrV1X2P/wejBY1jREVjb/xCGNWvxInK3cgW3cTWPzHBvKIaVaxb1/ZSwr2ofvOZPE8nd6kuM5zYj8IOS/jIaD4YVHYHxVljzLZO7lSvOLarxTT2+zTq0YFih+Nd1PuSWcOfGd5fk43qdBkHuVq64t6AmxSF+GFauqTKmYc1G6vPDvU0kdytX7LOr8Qmpr40WiWFFR6DOlnCjUZK7lSv22dX4suPLokViWNERiDMs9/yd3TaR3K1c8yCjmgsl/Wq0MAwrOgJr+x/6U8K+ozu52yZy7lZfYqv33M9K+oXoYWNY0RGIXWHtPHpyt3LNh0xqLpH04mhBGFZ0BHIZltX4oLYXkbuVa2IkUPPrks6P1oFhRUcgn2HNFPnK9J/m3K1cEyRQzeXdNWWBEiQMKxT/us5rpDUsMuJdJfndBblbi1Cb5rNvkHRW9NAwrOgI5F1hzSs7otsmkruVa77UVHOFpNNrdrhRXxhWdATW9h/1KWFfCud220TuTOxLbDrPvV3SY6KHg2FFR6CNFda8yh3dNpHcrVxzZ2w1vsH62LE72a59DGs7QnX/Pts7rK1G75wtf5pI7lbdORLV23sl/UBU57N+MazoCLS3wtqZmI9mdhY0dybmmktDq7lGkr+HGlowrFD86zpvaYU1L/4+3TaRc7dyzach1Vwr6aghG1ymLQxrGWrj1WnVsGZE/Cmis6G5M3G8ORLV8gckPSyqc7aE0eQ37j/7p4R9qPkfwZd220TO3epDrI1nfJ67z3UPLaywQvFPZku4EcVDu20iuVu55tiyav5ekmMaWjCsUPyTNqzZ4Pxey9tEcrdyzbVF1fyDpAcvWmno5zGsoYmWtdf6O6zNRn/Xbpvor/hQ2iTwUUkHR0vHsKIjsLb/qRrWbJTO4/F3E8ndyjXv+qj5uKSD+jw45jMY1ph0F2976oY1I+IseW8Tyd1afI5E1fiUpPtFdT7rF8OKjsDa/qfwKWFfot/TbRPP6VuB50IJfFrSAaEKxPEy0fx37n9VVljz435cZ1zkbmWbjWv1fFbSvaMlssKKjsBqvcPaivbF3TaR3K1cc3Km5l8leVUcWjCsUPzrOl/FFdY8BH8K5aTTM3KFBTWSvtDdshQKA8MKxY9hbYL/RzvjIncrz/z8UoYPSTCsPBPCSlZ9hTUfDW8NvdryTT6UeAI3Sto3WgaGFR2Btf2v0qeEfcn7hAAb14l9K/DcKAS+ImmfUVpeoFEMawFYFR5lhbU55Gd0xkXuVoWJuEEX/yHJ31gILRhWKH7eYS2I32bl1ZbNi1KXwC2S9qjb5freMKzoCKztnxVWv3h4e2jjCj9Qrp/cSTz1NUnhKScYVq65hGEtFg+/kLdxhf8iLSa7yaf/W9Kdo5VjWNERYIVVGgF/v80fVjgVgjIegf+VdKfxmu/XMobVj1Otp1hhLU/ayaZebYUfgbL8EFLXvE3SLtEKMazoCLDCGiICvs3Fq6zjh2iMNjYk8E1Jd4xmg2FFRwDDKonA7p1RPb+kEer2InCrpF17PTniQxjWiHCXaJotYX9oZ3VbwPAjT/pLbvrJr0vaLXoEGFZ0BFhhLRqBB3WrqtMWrcjzRQT+S9J3FLUwQGUMawCIAzbBCmtrmD5e+SUZPq0aMOatNPVVSXtGi8WwoiPACqtPBE7ptn8P6fMwz4xC4GZJe4/S8gKNYlgLwKrwKCustZB9wqVTFZ5WgT1dbE3gJkk7oiFhWNERYIW1WQTOl/Qzku6WK0Qrq+bfJN0jevQYVnQEMKydI3Bst6o6OldoVl7NFyXdK5oChhUdAQxrRsBnLXn799xcIUFNR+Bzkr47mgaGFR0BDMsEzu62f/vnCgdq5gj8s6T9oolgWNERWG3DOrxbVT0+VxhQswGBz0i6TzQZDCs6AqtpWP7Wv7d/LySnKtcE3ELNDZLuG60Ww4qOwOoZ1und9u+QXOhRsw2BT0g6MJoShhUdgdUxLJ9b5VXVj+VCjpqeBD4m6YE9nx3tMQxrNLRLNTzVxNELJb2AnKql5kSWSh+W5O9xhhYMKxT/us6nZlgndNu/R+XCjJolCHxIUvg2HsNaInIjVpmKYTkj2tu/Z47IiqbrErhe0mF1u1zfG4YVHYHpvcN6erf9I6cq19wqVfN3ko4obaS0PoZVSnDY+i2vsI7stn+PHRYJrSUhcJ0kxzi0YFih+CfxDsunUHr752OKw29VyRXOSam5RpLPzg8tGFYo/uYN68nd9i/8ZWyuME5SzfskhX94gmHlmlutbAmdj+OjX2xYlNUg8B5Jx0QPFcOKjsDa/lswrIslXUBOVa6JU0HNOySdVKGfLbvAsKIj0I5h+WW6kz/DtwW5QrYyat4i6dTo0WJY0RHIb1g+A8nbP6crUFaXwO9n+FoVhpVrAmbbEj6n2/6RU5VrnkSo+R1JPxHR8XyfGFZ0BHKusLzt8/aPnKpc8yNSzaWS/A9YaMGwQvGv6zx6heV75/xS/XnkVOWaGAnU/Iqki6J1YFjREcizwnpqt/0jpyrXnMiiJvof0//ngGFlmQ7f0hExKWxQ3v6RU5VrLmRT49NhXx4tCsOKjkDsCoucqlzxz6zmPEmvjBaIYUVHIMawnE/j5E9yqnLFP7OacyX9drRADCs6AnUNy7ee+MUpOVW54t6CmjMlvTFaKIYVHYF6hkVOVa5Yt6bmhyT9cbRoDCs6AuMb1nHd9o+cqlyxbk3NyZKujhaNYUVHYDzD2iHJlz+QU5Urxq2q8UkNPrEhtGBYofjXdT5UWgM5VbniOgU1D5f0t9EDwbCiIzDsCuuh3YqKnKpccZ2CGl/x5au+QguGFYp/sBXWrt32j3OqcsVzSmp8Tb2vqw8tGFYo/kEMi5yqXDGcqprvkvT56MFhWNERWH5LeP9u+0dOVa4YTlXN3pJujh4chhUdgeUMi5yqXHFbBTV3kfSN6IFiWNERWMywyKnKFa9VUXOrJL8nDS8YVngI1gjYLK3hnl3yJzlVueK1Kmo+J8lHZYcXDCs8BNsaFjlVuWK0imqul3RYhoFjWBmicLuG+RUWOVW5YrPKav5c0okZAGBYGaKw1rBeMbf92yeXPNSsKIE3SDorw9gxrAxRWGtYj+acqlxBKVTz7u7SWWeKt1p+rUuhCdePYYWHAAETJ2DDOlbS47pbZ05ocLwvkvTLGXRjWBmigIYpE5gZ1myMR0s6J8sWqyd46/3dns+O+lgGwzpUkj+FoEBgigT+bJP7HR8m6WmSfrjbMmYe+xMkXZVBYAbD2kvSVzLAQAMERiDwOkk/vkW7zrGzafnnESP0P0STR0q6boiGStvIYFgegw3LxkWBwNQILHIBqVMHZua1eyIQvgvgMxn0ZDEsbwm9NaRAYGoEfOmHTWuRst+ccTkfL7rsIemWaBHuP4thXSnpiRmAoAECAxPwdtDbwmWL3x/NVl3ftmwjBfX+U1Ka1V4Ww3Kex/kFUKkKgawEfPmHX7yXFh8nNDOumjld/yRp/1LxQ9XPYlhPkvSmoQZFOxBIRODukr48sJ7TJZ0hyf/dZeC2d27O57j7PPcUJYth7Snp31MQQQQEhiOwcw7WcC1/q6WDOtOycR0ydONde38kyQuKFCWLYRnG2zbJV0kBChEQWILAsyVdtkS9ZaqcMmdePmxvqPIySf5SfoqSybAc3EtTUEEEBMoJ+NA7n4M+9HZwO2UHSPItzf5xcmpp8dbzzaWNDFU/k2E9UNJHhhoY7UAgmMAV3YonUoYTPk+S5FublzWvgyV9NHIQ831nMizrujbTC74sQUJHkwSeIun1iZQvY17+fTwq0RjS5GHNmJyZLMiZYoWWdgj4ayw2iKzFn/r5x0mp/nnAJkJ9IW+qT++zrbDM7a2SnCxHgUCrBHys9e81JN6pF0d0PzPZH5f0B9nGkNGwjpH0rmyg0AOBngQ8d30II2UEAhkNy8N8laRnjTBemoTA2ARSfao29mBrt5/VsPzR7Psl7VsbCP1BoIDAn3YnixY0QdWtCGQ1LGv+SUmvIXwQaITADZJOk/ShRvQ2KTOzYRkoX4puclqtpOg0h9xNmX52wzL7q7PciTblicDYigj4JTsfFBUh7Fe5BcPySHzEhQ81o0AgG4FTJb0lm6ip6mnFsMz/xgYO65/qPGFcGxN4qaSfB049Ai0Zlqn4qqGtDvSvR46eVp3A2ZJeu+oQao+/NcMyn3MlXV4bFP1BoCPg0xf8lZW/gkh9Ai0alin52+ev5r1W/Qmz4j2+t7sE9ZMrziFs+K0aloH5XOvzugkUBpCOV4LA5yX9lqRLJH1jJUacdJAtG9YM6SO7r/F4mU6BwJAEZkblVxD+MyWYwBQMa4bwhM64fFQsBQIlBDCqEnoj1p2SYc0wHS7peEnHdT9j3yoyYnhouiIB5/pdJckXRzgJ1LeRU5IRmKJhzSP2bTy+/tvHxPqdl79MvSPTxZDJ5sOqyLlJkn+c2/fOzqh8nRUlOYGpG9Zm+HfrjMvmtVfyGCGvnIAvhLA5zUzqtvImaSGCwKoaVgRr+oQABAoJYFiFAKkOAQjUI4Bh1WNNTxCAQCEBDKsQINUhAIF6BDCseqzpCQIQKCSAYRUCpDoEIFCPAIZVjzU9QQAChQQwrEKAVIcABOoRwLDqsaYnCECgkACGVQiQ6hCAQD0CGFY91vQEAQgUEsCwCgFSHQIQqEcAw6rHmp4gAIFCAhhWIUCqQwAC9QhgWPVY0xMEIFBIAMMqBEh1CECgHgEMqx5reoIABAoJYFiFAKkOAQjUI4Bh1WNNTxCAQCEBDKsQINUhAIF6BDCseqzpCQIQKCSAYRUCpDoEIFCPAIZVjzU9QQAChQQwrEKAVIcABOoRwLDqsaYnCECgkACGVQiQ6hCAQD0CGFY91vQEAQgUEsCwCgFSHQIQqEcAw6rHmp4gAIFCAhhWIUCqQwAC9QhgWPVY0xMEIFBIAMMqBEh1CECgHgEMqx5reoIABAoJYFiFAKkOAQjUI4Bh1WNNTxCAQCEBDKsQINUhAIF6BDCseqzpCQIQKCSAYRUCpDoEIFCPAIZVjzU9QQAChQQwrEKAVIcABOoRwLDqsaYnCECgkACGVQiQ6hCAQD0CGFY91vQEAQgUEsCwCgFSHQIQqEcAw6rHmp4gAIFCAhhWIUCqQwAC9QhgWPVY0xMEIFBIAMMqBEh1CECgHgEMqx5reoIABAoJYFiFAKkOAQjUI4Bh1WNNTxCAQCEBDKsQINUhAIF6BDCseqzpCQIQKCSAYRUCpDoEIFCPAIZVjzU9QQAChQQwrEKAVIcABOoRwLDqsaYnCECgkACGVQiQ6hCAQD0CGFY91vQEAQgUEsCwCgFSHQIQqEcAw6rHmp4gAIFCAhhWIUCqQwAC9QhgWPVY0xMEIFBIAMMqBEh1CECgHgEMqx5reoIABAoJYFiFAKkOAQjUI4Bh1WNNTxCAQCEBDKsQINUhAIF6BP4Pk1F1SwISH8EAAAAASUVORK5CYII=";

    private static final String SHAPE_TYPE_ID = "_x0000_t80";

    private static final String contentType = "application/vnd.openxmlformats-officedocument.oleObject";

    private static final String SHAPE_TYPE_XML = "<v:shapetype id=\"" + SHAPE_TYPE_ID + "\" coordsize=\"21600,21600\" o:spt=\"75\" o:preferrelative=\"t\" "
            + "                      path=\"m@4@5l@4@11@9@11@9@5xe\" filled=\"f\" stroked=\"f\">\n"
            + "                        <v:stroke joinstyle=\"miter\"/>\n"
            + "                        <v:formulas>\n"
            + "                            <v:f eqn=\"if lineDrawn pixelLineWidth 0\"/>\n"
            + "                            <v:f eqn=\"sum @0 1 0\"/>\n"
            + "                            <v:f eqn=\"sum 0 0 @1\"/>\n"
            + "                            <v:f eqn=\"prod @2 1 2\"/>\n"
            + "                            <v:f eqn=\"prod @3 21600 pixelWidth\"/>\n"
            + "                            <v:f eqn=\"prod @3 21600 pixelHeight\"/>\n"
            + "                            <v:f eqn=\"sum @0 0 1\"/>\n"
            + "                            <v:f eqn=\"prod @6 1 2\"/>\n"
            + "                            <v:f eqn=\"prod @7 21600 pixelWidth\"/>\n"
            + "                            <v:f eqn=\"sum @8 21600 0\"/>\n"
            + "                            <v:f eqn=\"prod @7 21600 pixelHeight\"/>\n"
            + "                            <v:f eqn=\"sum @10 21600 0\"/>\n"
            + "                        </v:formulas>\n"
            + "                        <v:path o:extrusionok=\"f\" gradientshapeok=\"t\" o:connecttype=\"rect\"/>\n"
            + "                        <o:lock v:ext=\"edit\" aspectratio=\"t\"/>\n"
            + "                    </v:shapetype>\n";

    @Override
    protected boolean validate(File data) {
        return null != data && null != data;
    }

    @Override
    protected void afterRender(RenderContext<File> context) {
        super.clearPlaceholder(context, false);
    }

    @Override
    public void doRender(RenderContext<File> context) throws Exception {
        NiceXWPFDocument doc = context.getXWPFDocument();
        XWPFRun run = context.getRun();
        CTR ctr = run.getCTR();

        String uuidRandom = UUID.randomUUID().toString().replace("-", "") + ThreadLocalRandom.current().nextInt(1024);
        String shapeId = "_x0000_i21" + uuidRandom;
        String part = "/word/embeddings/oleObject1.bin";
        PictureRenderData icon = Pictures.ofBase64(ICON, PictureType.PNG)
                .size(100, 64)
                .create();
        byte[] image = icon.readPictureData();
        PictureType pictureType = icon.getPictureType();
        if (null == pictureType) {
            pictureType = PictureType.suggestFileType(image);
        }

        PictureStyle style = icon.getPictureStyle();
        if (null == style) {
            style = new PictureStyle();
        }
        double widthPt = Units.pixelToPoints(style.getWidth());
        double heightPt = Units.pixelToPoints(style.getHeight());
        String imageRId = doc.addPictureData(image, pictureType.type());

        FileReader fileReader = new FileReader(context.getData());
        PackagePartName partName = PackagingURIHelper.createPartName(part);


        Document doc2 = new Document();
        Section section = doc2.addSection();
        Paragraph para3 = section.addParagraph();
        DocPicture picture = new DocPicture(doc2);
        picture.loadImage(image);
        para3.appendOleObject(fileReader.readBytes(),picture, "mp4");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        doc2.saveToStream(byteArrayOutputStream, FileFormat.Docx);

        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        for (POIXMLDocumentPart relation : new XWPFDocument(new ByteArrayInputStream(byteArrayOutputStream.toByteArray())).getRelations()) {
            if (StrUtil.equals(relation.getPackagePart().getContentType(), "application/vnd.openxmlformats-officedocument.oleObject")) {
                IoUtil.copy(relation.getPackagePart().getInputStream(), arrayOutputStream);
            }
        }
        PackagePart packagePart = doc.getPackage().createPart(partName,contentType);
        try (OutputStream out = packagePart.getOutputStream()) {
            out.write(arrayOutputStream.toByteArray());
        } catch (IOException e) {
            throw new POIXMLException(e);
        }


        PackageRelationship ole = doc.getPackagePart().addRelationship(partName, TargetMode.INTERNAL,
                POIXMLDocument.PACK_OBJECT_REL_TYPE);
        String oleId = ole.getId();
        String wObjectXml = "<w:object xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\""
                + "             xmlns:v=\"urn:schemas-microsoft-com:vml\""
                + "             xmlns:o=\"urn:schemas-microsoft-com:office:office\""
                + "             xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\""
                + "             w:dxaOrig=\"1520\" w:dyaOrig=\"960\">\n" + SHAPE_TYPE_XML
                + "                    <v:shape id=\"" + shapeId + "\" type=\"#"
                + SHAPE_TYPE_ID + "\" alt=\"\" style=\"width:" + widthPt + "pt;height:" + heightPt
                + "pt;mso-width-percent:0;mso-height-percent:0;mso-width-percent:0;mso-height-percent:0\" o:ole=\"\">\n"
                + "                        <v:imagedata r:id=\"" + imageRId + "\" o:title=\"\"/>\n"
                + "                    </v:shape>\n"
                + "                    <o:OLEObject Type=\"Embed\" ProgID=\"Package\" ShapeID=\"" + shapeId
                + "\" DrawAspect=\"Icon\" ObjectID=\""+shapeId+"\" r:id=\""+ oleId + "\">\n"
                + "                     <o:FieldCodes>\\s</o:FieldCodes>\n"
                + "                    </o:OLEObject>"
                + "            </w:object>";

        org.w3c.dom.Document document2 = DocumentHelper.readDocument(new InputSource(new StringReader(wObjectXml)));
        ctr.set(XmlObject.Factory.parse(document2.getDocumentElement(), POIXMLTypeLoader.DEFAULT_XML_OPTIONS));
    }
}
