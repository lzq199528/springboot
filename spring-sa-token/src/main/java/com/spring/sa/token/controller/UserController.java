package com.spring.sa.token.controller;

import cn.dev33.satoken.basic.SaBasicUtil;
import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.stp.StpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuzhiqiang
 */
@RestController
@RequestMapping("/user/")
@Slf4j
public class UserController {

    public static final String username = "zhang";
    public static final String password = "123456";

    /**
     * 测试登录，浏览器访问： http://localhost:1010/user/doLogin?username=zhang&password=123456
     *
     * @param username
     * @param password
     * @return
     */
    @GetMapping("doLogin")
    public String doLogin(String username, String password) {
        // 此处仅作模拟示例，真实项目需要从数据库中查询数据进行比对
        if (username.equals(username) && password.equals(password)) {
            StpUtil.login(10001L);
            log.info("登录id:{}",StpUtil.getLoginIdDefaultNull());
            log.info("tokenInfo:{}",StpUtil.getTokenInfo());
            log.info("权限集合:{}", StpUtil.getPermissionList());
            log.info("角色集合:{}", StpUtil.getRoleList());
            return "登录成功";
        }
        return "登录失败";
    }

    /**
     * 查询登录状态，浏览器访问： http://localhost:8081/user/isLogin
     *
     * @return
     */
    @GetMapping("isLogin")
    public String isLogin() {
        SaBasicUtil.check("sa:123456");
        return "当前会话是否登录：" + StpUtil.isLogin();
    }

    /**
     * 检查登录
     * StpUtil.checkLogin(); 检验当前会话是否已经登录, 如果未登录，则抛出异常：`NotLoginException`
     * -1	NotLoginException.NOT_TOKEN	未能从请求中读取到token
     * -2	NotLoginException.INVALID_TOKEN	已读取到token，但是token无效
     * -3	NotLoginException.TOKEN_TIMEOUT	已读取到token，但是token已经过期
     * -4	NotLoginException.BE_REPLACED	已读取到token，但是token已被顶下线
     * -5	NotLoginException.KICK_OUT	已读取到token，但是token已被踢下线
     *
     * @return
     */
    @GetMapping("checkLogin")
    public String checkLogin() {
        try {
            StpUtil.checkLogin();
            return "登录成功";
        } catch (NotLoginException e) {
            switch (e.getType()) {
                case NotLoginException.NOT_TOKEN:
                    return "未能从请求中读取到token";
                case NotLoginException.INVALID_TOKEN:
                    return "已读取到token，但是token无效";
                case NotLoginException.TOKEN_TIMEOUT:
                    return "已读取到token，但是token已经过期";
                case NotLoginException.BE_REPLACED:
                    return "已读取到token，但是token已被顶下线";
                case NotLoginException.KICK_OUT:
                    return "已读取到token，但是token已被踢下线";
                default:
                    return "";
            }
        }

    }

    /**
     * 退出
     *
     * @return
     */
    @GetMapping("logout")
    public String logout() {
        // 当前会话注销登录
        StpUtil.logout();
        return "退出登录";
    }

    @GetMapping("kickout")
    public String kickout(Long id) {
        StpUtil.kickout(id);
        return "已下线";
    }

}