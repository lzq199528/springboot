package com.spring.sa.token;


import cn.dev33.satoken.SaManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author liuzhiqiang
 */
@SpringBootApplication
@Slf4j
public class SaTokenApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SaTokenApplication.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("SaTokenApplication服务器已启动完毕");
        log.info("启动成功：Sa-Token配置如下：{}" + SaManager.getConfig());
    }
}
